#include <Wire.h>  
#include <Adafruit_BME280.h> 
#include <Adafruit_Sensor.h> 
const int  buttonPin = 23;    
const int ledPin = 2;

Adafruit_BME280 bme; //penggunaan I2C
void setup() {
  Serial.begin(9600);
  pinMode(buttonPin, INPUT);
  pinMode(ledPin, OUTPUT);
  if (!bme.begin(0x76)) {
    Serial.println("No BME!!!");
    while (1);
  }
}

void loop() {  
Serial.print("Curah Hujan = ");
Serial.print (digitalRead(ledPin));
Serial.println (" mm");

Serial.print("Suhu = "); 
Serial.print(bme.readTemperature());
Serial.println(" *C"); 

Serial.print("Kelembaban = "); 
Serial.print(bme.readHumidity()); 
Serial.println(" %"); 

Serial.print("Pressure = ");
Serial.print(bme.readPressure() / 100.0F);
Serial.println(" hPa");

delay(1000);
//Serial.println(); delay(1000);


}