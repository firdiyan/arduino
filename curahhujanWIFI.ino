
#include <WiFi.h>
#include <HTTPClient.h>
const int RAIN_INT = 21;  
#define LED_PIN 13
volatile int myTotal = 0;
String apiKey = "CZCUR7OGDQX0YCPH";
const char* ssid     = "LAB. ELEKTRONIKA";         
const char* password = "";    
const char* serverName = "http://api.thingspeak.com/update";

unsigned long lastTime = 0;
unsigned long timerDelay = 5000;

void myCount ();

void setup() {
 

  WiFi.begin(ssid, password);
  Serial.println("Connecting");
  while(WiFi.status() != WL_CONNECTED) {
    delay(2000);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());
 
  Serial.println("Timer set to 10 seconds (timerDelay variable), it will take 10 seconds before publishing the first reading.");
  pinMode(RAIN_INT, INPUT);
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);
  attachInterrupt(digitalPinToInterrupt(RAIN_INT), myCount, RISING);
  Serial.begin(115200);
  
}


void myCount() {
 myTotal +=1;
 digitalWrite(LED_PIN, HIGH);
}

void loop() {
 Serial.print("Jumlah tip: ");
 Serial.print(myTotal);
  Serial.print("  Curah Hujan  ");
 Serial.println(myTotal * 0.1287695) ;
 delay(1000);
 digitalWrite(LED_PIN, LOW);
  if ((millis() - lastTime) > timerDelay) {

    if(WiFi.status()== WL_CONNECTED){
      HTTPClient http;
      
      http.begin(serverName);
     
      http.addHeader("Content-Type", "application/x-www-form-urlencoded");
  
      String httpRequestData = "api_key=" + apiKey + "&field1=" + String(myTotal);           
     
      int httpResponseCode = http.POST(httpRequestData);
      
      
     
      Serial.print("HTTP Response code: ");
      Serial.println(httpResponseCode);
      
      http.end();
    }
    else {
      Serial.println("WiFi Disconnected");
    }
    lastTime = millis();
  }
}